package com.example.matrimony.model;

import java.io.Serializable;

public class UserModel implements Serializable {
    int UserId;
    String Name;
    String FatherName;
    String Email;
    int Gender;
    String SurName;
    String Language;
    String City;

    String Hobbies;
    String Dob;
    String PhoneNumber;
    int LanguageId;
    int CityId;

    int IsFavorite;

    public int getIsFavorite() {
        return IsFavorite;
    }

    public void setIsFavorite(int isFavorite) {
        IsFavorite = isFavorite;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getLanguage() {
        return Language;
    }

    public void setLanguage(String language) {
        Language = language;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getFatherName() {
        return FatherName;
    }

    public void setFatherName(String fatherName) {
        FatherName = fatherName;
    }

    public int getGender() {
        return Gender;
    }

    public void setGender(int gender) {
        Gender = gender;
    }

    public String getSurName() {
        return SurName;
    }

    public void setSurName(String surName) {
        SurName = surName;
    }

    public String getHobbies() {
        return Hobbies;
    }

    public void setHobbies(String hobbies) {
        Hobbies = hobbies;
    }

    public String getDob() {
        return Dob;
    }

    public void setDob(String dob) {
        Dob = dob;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public int getLanguageId() {
        return LanguageId;
    }

    public void setLanguageId(int languageId) {
        LanguageId = languageId;
    }

    public int getCityId() {
        return CityId;
    }

    public void setCityId(int cityId) {
        CityId = cityId;
    }

    @Override
    public String toString() {
        return "UserModel{" +
                "UserId=" + UserId +
                ", Name='" + Name + '\'' +
                ", FatherName='" + FatherName + '\'' +
                ", Email='" + Email + '\'' +
                ", Gender=" + Gender +
                ", SurName='" + SurName + '\'' +
                ", Language='" + Language + '\'' +
                ", City='" + City + '\'' +
                ", Hobbies='" + Hobbies + '\'' +
                ", Dob='" + Dob + '\'' +
                ", PhoneNumber='" + PhoneNumber + '\'' +
                ", LanguageId=" + LanguageId +
                ", CityId=" + CityId +
                ", IsFavorite=" + IsFavorite +
                '}';
    }
}
