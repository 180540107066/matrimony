package com.example.matrimony.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.matrimony.model.CityModel;

import java.util.ArrayList;

public class TblMstCity extends MyDatabase {
    public static final String TABLE_NAME = "TblMstCity";
    public static final String CITY_ID = "CityId";
    public static final String NAME = "Name";

    public TblMstCity(Context context) {
        super(context);
    }

    public ArrayList<CityModel> getCityList() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<CityModel> list = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        CityModel cityModel = new CityModel();
        cityModel.setName("Select one City");
        list.add(0, cityModel);

        for (int i = 0; i < cursor.getCount(); i++) {
            cityModel = new CityModel();
            cityModel.setCityId(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
            cityModel.setName(cursor.getString(cursor.getColumnIndex(NAME)));
            list.add(cityModel);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }


    public CityModel getCityById(int id) {
        SQLiteDatabase db = getReadableDatabase();
        CityModel model = new CityModel();
        String query = "SELECT * FROM " + TABLE_NAME + " Where " + CITY_ID + "=?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(id)});
        cursor.moveToFirst();
        model.setCityId(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
        model.setName(cursor.getString(cursor.getColumnIndex(NAME)));
        cursor.close();
        db.close();
        return model;
    }

}
