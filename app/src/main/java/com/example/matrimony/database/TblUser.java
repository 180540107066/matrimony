package com.example.matrimony.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.matrimony.model.UserModel;
import com.example.matrimony.util.Utils;

import java.util.ArrayList;

public class TblUser extends MyDatabase {

    public static final String TABLE_NAME = "TblUser";
    public static final String USER_ID = "UserId";
    public static final String NAME = "Name";
    public static final String FATHER_NAME = "FatherName";
    public static final String SUR_NAME = "SurName";
    public static final String EMAIL = "Email";
    public static final String GENDER = "Gender";
    public static final String HOBBIES = "Hobbies";
    public static final String DOB = "Dob";
    public static final String PHONE_NUMBER = "PhoneNumber";
    public static final String LANGUAGE_ID = "LanguageId";
    public static final String CITY_ID = "CityId";
    public static final String IS_FAVORITE = "IsFavority";
    /* query column */
    public static final String LANGUAGE = "Language";
    public static final String CITY = "City";
    public static final String AGE = "Age";


    public TblUser(Context context) {
        super(context);
    }

    public ArrayList<UserModel> getUserList() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserModel> list = new ArrayList<>();
//        String query="SELECT * FROM  "+TABLE_NAME;
        String query = "SELECT " +
                " UserId," +
                " TblUser.Name as Name," +
                " FatherName," +
                " SurName," +
                " Email," +
                " IsFavority," +
                " Gender," +
                " Dob," +
                " PhoneNumber," +
                " Hobbies, " +
                " TblMstLanguage.LanguageId," +
                " TblMstCity.CityId," +
                " TblMstLanguage.Name as Language," +
                " TblMstCity.Name as City" +

                " FROM " +
                " TblUser " +
                " INNER JOIN TblMstLanguage ON TblUser.LanguageId = TblMstLanguage.LanguageId " +
                " INNER JOIN  TblMstCity ON TblUser.CityId = TblMstCity.CityId";
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        Log.d("userList::1::", "" + cursor.getCount());
        for (int i = 0; i < cursor.getCount(); i++) {
            list.add(getCreateModelUsingCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }

    public UserModel getCreateModelUsingCursor(Cursor cursor) {
        UserModel model = new UserModel();
        model.setUserId(cursor.getInt(cursor.getColumnIndex(USER_ID)));
        model.setCityId(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
        model.setLanguageId(cursor.getInt(cursor.getColumnIndex(LANGUAGE_ID)));
        model.setGender(cursor.getInt(cursor.getColumnIndex(GENDER)));
        model.setDob(Utils.getDateToDisplay(cursor.getString(cursor.getColumnIndex(DOB))));
//        model.setDob(Utils.getFormatedDateToInsert(cursor.getString(cursor.getColumnIndex(DOB))));
        model.setFatherName(cursor.getString(cursor.getColumnIndex(FATHER_NAME)));
        model.setHobbies(cursor.getString(cursor.getColumnIndex(HOBBIES)));
        model.setName(cursor.getString(cursor.getColumnIndex(NAME)));
        model.setEmail(cursor.getString(cursor.getColumnIndex(EMAIL)));
        model.setPhoneNumber(cursor.getString(cursor.getColumnIndex(PHONE_NUMBER)));
        model.setSurName(cursor.getString(cursor.getColumnIndex(SUR_NAME)));
        model.setCity(cursor.getString(cursor.getColumnIndex(CITY)));
        model.setLanguage(cursor.getString(cursor.getColumnIndex(LANGUAGE)));
        model.setIsFavorite(cursor.getInt(cursor.getColumnIndex(IS_FAVORITE)));
        return model;
    }




    public UserModel getUserById(int id) {
        SQLiteDatabase db = getReadableDatabase();
        UserModel model = new UserModel();
        String query = " SELECT * FROM " + TABLE_NAME + " WHERE " + USER_ID + "= ?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(id)});
        cursor.moveToFirst();
        model = getCreateModelUsingCursor(cursor);
        cursor.close();
        db.close();
        return model;
    }

    public ArrayList<UserModel> getUserListByGender(int gender) {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserModel> list = new ArrayList<>();
//        String query = " SELECT * FROM " + TABLE_NAME + " WHERE " + GENDER + "=?";
        String query = "SELECT " +
                "UserId," +
                "TblUser.Name as Name," +
                "FatherName," +
                "SurName," +
                "Email," +
                "IsFavority," +
                "Gender," +
                "Dob," +
                "PhoneNumber," +
                "Hobbies," +
                "TblMstLanguage.LanguageId," +
                "TblMstCity.CityId," +
                "TblMstLanguage.Name as Language," +
                "TblMstCity.Name as City" +

                " FROM " +
                " TblUser " +
                " INNER JOIN TblMstLanguage ON TblUser.LanguageId = TblMstLanguage.LanguageId " +
                " INNER JOIN  TblMstCity ON TblUser.CityId = TblMstCity.CityId" +

                " WHERE " + "GENDER  =?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(gender)});
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            list.add(getCreateModelUsingCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }

    public ArrayList<UserModel> getFavoriteUserList() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserModel> list = new ArrayList<>();
//        String query = " SELECT * FROM " + TABLE_NAME + " WHERE " + GENDER + "=?";
        String query = "SELECT " +
                " UserId, " +
                " TblUser.Name as Name, " +
                " FatherName, " +
                " SurName, " +
                " Email, " +
                " IsFavority, " +
                " Gender, " +
                " Dob, " +
                " PhoneNumber, " +
                " Hobbies, " +
                " TblMstLanguage.LanguageId, " +
                " TblMstCity.CityId, " +
                " TblMstLanguage.Name as Language, " +
                " TblMstCity.Name as City " +

                " FROM " +
                " TblUser " +
                " INNER JOIN TblMstLanguage ON TblUser.LanguageId = TblMstLanguage.LanguageId " +
                " INNER JOIN  TblMstCity ON TblUser.CityId = TblMstCity.CityId" +

                " WHERE " + "IsFavority  =?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(1)});
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            list.add(getCreateModelUsingCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }

    //    select query for insert user form database.
    public long insertUser(String name, String fatherName, String surName, int gender, String email,
                           String date, String phoneNumber, String hobbies, int languageId, int isFavorite, int cityId) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(NAME, name);
        cv.put(FATHER_NAME, fatherName);
        cv.put(SUR_NAME, surName);
        cv.put(EMAIL, email);
        cv.put(GENDER, gender);
        cv.put(HOBBIES, hobbies);
        cv.put(DOB, date);
        cv.put(PHONE_NUMBER, phoneNumber);
        cv.put(LANGUAGE_ID, languageId);
        cv.put(CITY_ID, cityId);
        cv.put(IS_FAVORITE, isFavorite);
        long lastInsertedID = db.insert(TABLE_NAME, null, cv);
        db.close();
        return lastInsertedID;
    }

    //    select query for update user form database.
    public int updateUserById(String name, String fatherName, String surName, int gender, String email,
                              String hobbies, String date, String phoneNumber, int languageId, int cityId, int isFavorite, int userId) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(NAME, name);
        cv.put(FATHER_NAME, fatherName);
        cv.put(SUR_NAME, surName);
        cv.put(EMAIL, email);
        cv.put(GENDER, gender);
        cv.put(HOBBIES, hobbies);
        cv.put(DOB, date);
        cv.put(PHONE_NUMBER, phoneNumber);
        cv.put(LANGUAGE_ID, languageId);
        cv.put(CITY_ID, cityId);
        cv.put(IS_FAVORITE, isFavorite);
        int lastUpdateID = db.update(TABLE_NAME, cv, USER_ID + " =?", new String[]{String.valueOf(userId)});
        db.close();
        return lastUpdateID;
    }

    //    select query for delete user form database.
    public int deleteUserById(int userId) {
        SQLiteDatabase db = getWritableDatabase();
        int detetedUserId = db.delete(TABLE_NAME, USER_ID + " =?", new String[]{String.valueOf(userId)});
        db.close();
        return detetedUserId;
    }

    public int updateFavoriteStatus(int isFavorite, int userId) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(IS_FAVORITE, isFavorite);
        cv.put(USER_ID, userId);
        int lastUpdateID = db.update(TABLE_NAME, cv, USER_ID + " =?", new String[]{String.valueOf(userId)});
        db.close();
        return lastUpdateID;
    }
}
