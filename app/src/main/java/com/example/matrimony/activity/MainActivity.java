package com.example.matrimony.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.cardview.widget.CardView;

import com.example.matrimony.R;
import com.example.matrimony.database.MyDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {


    @BindView(R.id.cvRegistration)
    CardView cvRegistration;
    @BindView(R.id.cvList)
    CardView cvList;
    @BindView(R.id.cvSearch)
    CardView cvSearch;
    @BindView(R.id.cvFavorite)
    CardView cvFavorite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.lbl_dashboard), false);
        new MyDatabase(this).getWritableDatabase();
    }

    @OnClick(R.id.cvRegistration)
    public void onCvRegistrationClicked() {
        Intent intent = new Intent(this, AddUserActvity.class);
        startActivity(intent);
    }

    @OnClick(R.id.cvList)
    public void onCvListClicked() {
        Intent intent = new Intent(this, ActivityUserListByGender.class);
        startActivity(intent);
    }

    @OnClick(R.id.cvSearch)
    public void onCvSearchClicked() {
        Intent intent = new Intent(this, ActivitySearchUser.class);
        startActivity(intent);
    }

    @OnClick(R.id.cvFavorite)
    public void onCvFavoriteClicked() {
        Intent intent = new Intent(this, FavoriteUserActivity.class);
        startActivity(intent);
    }
}