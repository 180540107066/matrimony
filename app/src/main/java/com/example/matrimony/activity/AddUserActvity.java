package com.example.matrimony.activity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.Spinner;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;

import com.example.matrimony.R;
import com.example.matrimony.adapter.CityAdapter;
import com.example.matrimony.adapter.LanguageAdapter;
import com.example.matrimony.database.TblMstCity;
import com.example.matrimony.database.TblMstLanguage;
import com.example.matrimony.database.TblUser;
import com.example.matrimony.model.CityModel;
import com.example.matrimony.model.LanguageModel;
import com.example.matrimony.model.UserModel;
import com.example.matrimony.util.Constant;
import com.example.matrimony.util.Utils;
import com.google.android.material.radiobutton.MaterialRadioButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddUserActvity extends BaseActivity {


    @BindView(R.id.etName)
    TextInputEditText etName;
    @BindView(R.id.etFatherName)
    TextInputEditText etFatherName;
    @BindView(R.id.etSurName)
    TextInputEditText etSurName;
    @BindView(R.id.rbMale)
    MaterialRadioButton rbMale;
    @BindView(R.id.rbFemale)
    MaterialRadioButton rbFemale;
    @BindView(R.id.rgGender)
    RadioGroup rgGender;
    @BindView(R.id.etDob)
    TextInputEditText etDob;
    @BindView(R.id.etPhoneNumber)
    TextInputEditText etPhoneNumber;
    @BindView(R.id.etEmailAddress)
    TextInputEditText etEmailAddress;
    @BindView(R.id.spCity)
    Spinner spCity;
    @BindView(R.id.spLanguage)
    Spinner spLanguage;
    @BindView(R.id.chbCricket)
    CheckBox chbCricket;
    @BindView(R.id.chbFootBall)
    CheckBox chbFootBall;
    @BindView(R.id.chbHockey)
    CheckBox chbHockey;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    String starting = "1995-07-19T12:00:00";

    Date date;

    CityAdapter cityAdapter;
    LanguageAdapter languageAdapter;

    ArrayList<CityModel> cityList = new ArrayList<>();
    ArrayList<LanguageModel> languageList = new ArrayList<>();
    UserModel userModel;
    private int screenLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acivity_add_user);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.lbl_addUser), true);
        setDateToView();
        getDataForUpdate();
    }

    public String getHobbies() {
        String hobbies = " ";
        if (chbCricket.isChecked() && (chbFootBall.isChecked() || chbHockey.isChecked())) {
            hobbies += chbCricket.getText().toString() + ",";
        } else if (chbCricket.isChecked()) {
            hobbies += chbCricket.getText().toString() + " ";
        }

        if ((chbFootBall.isChecked() && chbHockey.isChecked())) {
            hobbies += chbFootBall.getText().toString() + ",";
        } else if (chbFootBall.isChecked()) {
            hobbies += chbFootBall.getText().toString() + " ";
        }


        if ((chbHockey.isChecked())) {
            hobbies += chbHockey.getText().toString() + " ";
        }
        return hobbies;
    }

    @SuppressWarnings("SingleStatementInBlock")
    void getDataForUpdate() {
        if (getIntent().hasExtra(Constant.USER_OBJECT)) {
            userModel = (UserModel) getIntent().getSerializableExtra(Constant.USER_OBJECT);

            getSupportActionBar().setTitle(R.string.lbl_add_user);
            etName.setText(userModel.getName());
            etFatherName.setText(userModel.getFatherName());
            etSurName.setText(userModel.getSurName());
            etEmailAddress.setText(userModel.getEmail());
            etPhoneNumber.setText(userModel.getPhoneNumber());
            etDob.setText(userModel.getDob());
            if (userModel.getGender() == Constant.MALE) {
                rbMale.setChecked(true);
            } else {
                rbFemale.setChecked(true);
            }
            spCity.setSelection(getSelectedPositionFromCityId(userModel.getCityId()));
            spLanguage.setSelection(getSelectedPositionFromLanguageId(userModel.getLanguageId()));
        }
    }

    int getSelectedPositionFromCityId(int cityId) {
        for (int i = 0; i < cityList.size(); i++) {
            if (cityList.get(i).getCityId() == cityId) {
                return i;
            }
        }
        return 0;
    }

    int getSelectedPositionFromLanguageId(int languageId) {
        for (int i = 0; i < languageList.size(); i++) {
            if (languageList.get(i).getLanguageId() == languageId) {
                return i;
            }
        }
        return 0;
    }

    //   List of item set spinner
    void setSpinnerAdapter() {
        cityList.addAll(new TblMstCity(this).getCityList());
        languageList.addAll(new TblMstLanguage(this).getLanguages());

        cityAdapter = new CityAdapter(this, cityList);
        languageAdapter = new LanguageAdapter(this, languageList);

        spCity.setAdapter(cityAdapter);
        spLanguage.setAdapter(languageAdapter);
        FragmentTransaction ft;
//        ft = getSupportFragmentManager().beginTransaction();
//        ft.add(R.id.screen_Layout, UserListFragment.getInstance());
    }

    void setDateToView() {
        final Calendar newCalendar = Calendar.getInstance();
        etDob.setText(String.format("%02d", newCalendar.get(Calendar.DAY_OF_MONTH)) + "/" +
                String.format("%02d", newCalendar.get(Calendar.MONTH) + 1) + "/" +
                String.format("%02d", newCalendar.get(Calendar.YEAR)));
        setSpinnerAdapter();
//        final Calendar newCalendar = Calendar.getInstance();
//        etDob.setText(String.format("%02d",newCalendar.get(Calendar.DAY_OF_MONTH))+"/"+ String.format("%02d",newCalendar.get(Calendar.MONTH)+1)+"/"+ newCalendar.get(Calendar.YEAR));
//        setSpinnerAdapter();
    }


    //    Date use for DatePicker
    @OnClick(R.id.etDob)
    public void onEtDobClicked() {
        final Calendar newCalendar = Calendar.getInstance();
        Date date = Utils.getDateFromString(starting);
        newCalendar.setTimeInMillis(date.getTime());
        DatePickerDialog datePickerDialog = new DatePickerDialog(AddUserActvity.this, (datePicker, year, month, day_of_month) -> {
            etDob.setText(String.format("%02d", day_of_month) + "/" + String.format("%02d", (month + 1)) + "/" + year);

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();

    }


    @OnClick(R.id.btnSubmit)
    public void onBtnSubmitClicked() {

        if (isValidUser()) {
            if (userModel == null) {
                long lastInsertedId = new TblUser(getApplicationContext()).insertUser(etName.getText().toString(),
                        etFatherName.getText().toString(),
                        etSurName.getText().toString(), rbMale.isChecked() ? Constant.MALE : Constant.FEMALE,
                        etEmailAddress.getText().toString(),
                        Utils.getFormatedDateToInsert(etDob.getText().toString()), etPhoneNumber.getText().toString(),
                        getHobbies(),
                        cityList.get(spCity.getSelectedItemPosition()).getCityId(), 0,
                        languageList.get(spLanguage.getSelectedItemPosition()).getLanguageId());
                showToast(lastInsertedId > 0 ? "User Insert Successfully" : "Something went wrong ");
            } else {
                long lastInsertedId = new TblUser(getApplicationContext()).updateUserById(etName.getText().toString(),
                        etFatherName.getText().toString(),
                        etSurName.getText().toString(), rbMale.isChecked() ? Constant.MALE : Constant.FEMALE,
                        etEmailAddress.getText().toString(), getHobbies(),
                        Utils.getFormatedDateToInsert(etDob.getText().toString()), etPhoneNumber.getText().toString(),
                        cityList.get(spCity.getSelectedItemPosition()).getCityId(), userModel.getIsFavorite(),
                        languageList.get(spLanguage.getSelectedItemPosition()).getLanguageId(), userModel.getUserId());
                showToast(lastInsertedId > 0 ? "User Update Successfully" : "Something went wrong ");
            }

          /*  Intent intent=new Intent(AddUserActvity.this,ActivityUserListByGender.class);
            startActivity(intent);
            finish();*/
        }
    }

//      Validation for add_user form

    boolean isValidUser() {
        boolean isValid = true;

        if (TextUtils.isEmpty(etName.getText().toString())) {
            isValid = false;
            etName.setError(getString(R.string.error_Enter_Name));
        }
        if (TextUtils.isEmpty(etFatherName.getText().toString())) {
            isValid = false;
            etFatherName.setError(getString(R.string.error_Enter_FatherName));
        }
        if (TextUtils.isEmpty(etSurName.getText().toString())) {
            isValid = false;
            etSurName.setError(getString(R.string.error_Enter_SurName));
        }


        //        phone Number
        if (TextUtils.isEmpty(etPhoneNumber.getText())) {
            etPhoneNumber.setError(getString(R.string.error_phone_number));
            isValid = false;
        } else {
            String phoneNumber = etPhoneNumber.getText().toString();
            if (phoneNumber.length() < 10) {
                etPhoneNumber.setError(getString(R.string.error_valid_phone_number));
                isValid = false;
            }
        }

        //        Email validation
        if (TextUtils.isEmpty(etEmailAddress.getText())) {
            etEmailAddress.setError(getString(R.string.error_email_address));
            isValid = false;
        } else {
            String email = etEmailAddress.getText().toString().trim();
            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
            if (!email.matches(emailPattern)) {
                etEmailAddress.setError(getString(R.string.error_valid_email));
                isValid = false;
            }
        }

        if (spCity.getSelectedItemPosition() == 0) {
            isValid = false;
            showToast(getString(R.string.error_select_city));
        }

        if (spLanguage.getSelectedItemPosition() == 0) {
            isValid = false;
            showToast(getString(R.string.error_select_language));
        }

//        Hobbies validation
        if (!(chbCricket.isChecked() || chbFootBall.isChecked() || chbHockey.isChecked())) {
            isValid = false;
            showToast("Select One hobbies");
        }

        return isValid;
    }
}
