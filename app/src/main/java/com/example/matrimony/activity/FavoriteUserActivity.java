package com.example.matrimony.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.matrimony.R;
import com.example.matrimony.adapter.UserListAdapter;
import com.example.matrimony.database.TblUser;
import com.example.matrimony.model.UserModel;
import com.example.matrimony.util.Constant;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FavoriteUserActivity extends BaseActivity {

    @BindView(R.id.rcvUserList)
    RecyclerView rcvUserList;

    ArrayList<UserModel> userList = new ArrayList<>();
    UserListAdapter adapter;
    @BindView(R.id.tvNoDataFound)
    TextView tvNoDataFound;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        ButterKnife.bind(this);
        setUpActionBar("Favorite User", true);
        setAdapter();

    }

    void showAlertDialog(int position) {
        AlertDialog alertDialog = new AlertDialog.Builder(FavoriteUserActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Are You Sure To Delete User");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                (dialog, which) -> {
                    int deleteUserId = new TblUser(FavoriteUserActivity.this).deleteUserById(userList.get(position).getUserId());
                    if (deleteUserId > 0) {
                        Toast.makeText(FavoriteUserActivity.this, "Delete User Successfully", Toast.LENGTH_SHORT).show();
                        userList.remove(position);
                        adapter.notifyItemRemoved(position);
                        adapter.notifyItemRangeChanged(0, userList.size());
                        CheckAndVisibleView();
                    } else {
                        Toast.makeText(FavoriteUserActivity.this, "Something Went to wrong", Toast.LENGTH_SHORT).show();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                (dialog, which) -> dialog.dismiss());
        alertDialog.show();
    }


    void setAdapter() {
        rcvUserList.setLayoutManager(new GridLayoutManager(this, 1));
        userList.addAll(new TblUser(this).getFavoriteUserList());
//       Log.d("userList::1::",""+userList.size());
        adapter = new UserListAdapter(this, userList, new UserListAdapter.OnViewClickListener() {
            @Override
            public void OnDeleteClick(int position) {
                showAlertDialog(position);
            }

            @Override
            public void onFavoriteClick(int position) {
                int lastUpdatedUserId = new TblUser(FavoriteUserActivity.this).updateFavoriteStatus(0, userList.get(position).getUserId());
                if (lastUpdatedUserId > 0) {
                    sendFavoriteChangeBroadCast(userList.get(position));
                    userList.remove(position);
                    adapter.notifyItemRemoved(position);
                    adapter.notifyItemChanged(0, userList.size());
                }
            }

            @Override
            public void OnItemClick(int position) {

            }
        });
//        Log.d("userList::2::",""+adapter.getItemCount());
        rcvUserList.setAdapter(adapter);
    }

    void sendFavoriteChangeBroadCast(UserModel userModel) {
        Intent intent = new Intent(Constant.FAVORITE_CHANGE_FILTER);
        intent.putExtra(Constant.USER_ID, userModel);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    void CheckAndVisibleView() {
        if (userList.size() > 0) {
            tvNoDataFound.setVisibility(View.GONE);
            rcvUserList.setVisibility(View.VISIBLE);
        } else {
            tvNoDataFound.setVisibility(View.VISIBLE);
            rcvUserList.setVisibility(View.GONE);
        }
    }
}
