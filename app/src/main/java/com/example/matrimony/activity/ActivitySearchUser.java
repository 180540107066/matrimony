package com.example.matrimony.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.matrimony.R;
import com.example.matrimony.adapter.UserListAdapter;
import com.example.matrimony.database.TblUser;
import com.example.matrimony.model.UserModel;
import com.example.matrimony.util.Constant;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivitySearchUser extends BaseActivity {

    @BindView(R.id.etUserSearch)
    EditText etUserSearch;
    @BindView(R.id.rcvUsers)
    RecyclerView rcvUsers;

    @BindView(R.id.tvNoDataFound)
    TextView tvNoDataFound;


    ArrayList<UserModel> userList = new ArrayList<>();
    ArrayList<UserModel> tempUserList = new ArrayList<>();
    UserListAdapter adapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_user);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.lbl_search_user), true);
        setAdapter();
        setSearchUser();

    }

    void showAlertDialog(int position) {
        AlertDialog alertDialog = new AlertDialog.Builder(ActivitySearchUser.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Are You Sure To Delete User");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                (dialog, which) -> {
                    int deleteUserId = new TblUser(ActivitySearchUser.this).deleteUserById(userList.get(position).getUserId());
                    if (deleteUserId > 0) {
                        Toast.makeText(ActivitySearchUser.this, "Delete User Successfully", Toast.LENGTH_SHORT).show();
                        userList.remove(position);
                        adapter.notifyItemRemoved(position);
                        adapter.notifyItemRangeChanged(0, userList.size());
                        CheckAndVisibleView();
                    } else {
                        Toast.makeText(ActivitySearchUser.this, "Something Went to wrong", Toast.LENGTH_SHORT).show();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                (dialog, which) -> dialog.dismiss());
        alertDialog.show();
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.ivFavoriteUser) {
            Intent intent = new Intent(this, FavoriteUserActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    void resetAdapter() {
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }


    }

    void setSearchUser() {
        etUserSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tempUserList.clear();
                if (charSequence.toString().length() > 0) {
                    for (int j = 0; j < userList.size(); j++) {
                        if (userList.get(j).getName().toLowerCase().contains(charSequence.toString().toLowerCase())
                                || userList.get(j).getFatherName().toLowerCase().contains(charSequence.toString().toLowerCase())
                                || userList.get(j).getSurName().toLowerCase().contains(charSequence.toString().toLowerCase())
                                || userList.get(j).getEmail().toLowerCase().contains(charSequence.toString().toLowerCase())
                                || userList.get(j).getPhoneNumber().toLowerCase().contains(charSequence.toString().toLowerCase())) {
                            tempUserList.add(userList.get(j));
                        }
                    }
                }
                if (charSequence.toString().length() == 0 && tempUserList.size() == 0) {
                    tempUserList.addAll(userList);
                }
                resetAdapter();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    void setAdapter() {
        rcvUsers.setLayoutManager(new GridLayoutManager(this, 1));
        userList.addAll(new TblUser(this).getUserList());
        tempUserList.addAll(userList);
        adapter = new UserListAdapter(this, tempUserList, new UserListAdapter.OnViewClickListener() {
            @Override
            public void OnDeleteClick(int position) {
                showAlertDialog(position);
            }

            @Override
            public void onFavoriteClick(int position) {
                int lastUpdatedUserId = new TblUser(ActivitySearchUser.this).updateFavoriteStatus(userList.get(position).getIsFavorite() == 0 ? 1 : 0, userList.get(position).getUserId());
                if (lastUpdatedUserId > 0) {
                    userList.get(position).setIsFavorite(userList.get(position).getIsFavorite() == 0 ? 1 : 0);
                    adapter.notifyItemChanged(position);
                }
            }

            @Override
            public void OnItemClick(int position) {

            }
        });
        rcvUsers.setAdapter(adapter);
    }

    void sendFavoriteChangeBroadCast(UserModel userModel) {
        Intent intent = new Intent(Constant.FAVORITE_CHANGE_FILTER);
        intent.putExtra(Constant.USER_ID, userModel);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    void CheckAndVisibleView() {
        if (userList.size() > 0) {
            tvNoDataFound.setVisibility(View.GONE);
            rcvUsers.setVisibility(View.VISIBLE);
        } else {
            tvNoDataFound.setVisibility(View.VISIBLE);
            rcvUsers.setVisibility(View.GONE);
        }
    }
}
