package com.example.matrimony.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.matrimony.R;
import com.example.matrimony.model.UserModel;
import com.example.matrimony.util.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.UserHolder> {

    Context context;
    ArrayList<UserModel> userList;

    OnViewClickListener onViewClickListener;

    public UserListAdapter(Context context, ArrayList<UserModel> userList, OnViewClickListener onViewClickListener) {
        this.context = context;
        this.userList = userList;
        this.onViewClickListener = onViewClickListener;
    }

    @NonNull
    @Override
    public UserHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UserHolder(LayoutInflater.from(context).inflate(R.layout.view_row_user_list, null));
    }

    @Override
    public void onBindViewHolder(@NonNull UserHolder holder, int position) {
        holder.tvFullName.setText(userList.get(position).getName() + " " + userList.get(position).getSurName());
        holder.tvLanguage.setText(userList.get(position).getLanguage() + " | " + userList.get(position).getCity());
//        holder.tvDob.setText(userList.get(position).getDob());
//
        String[] date = userList.get(position).getDob().split("/");
        String age = Utils.getAge(Integer.parseInt(date[2]), Integer.parseInt(date[1]), Integer.parseInt(date[0]));
        holder.tvAge.setText("Age : " + age);
        holder.ivFavoriteUser.setImageResource(userList.get(position).getIsFavorite() == 0 ? R.drawable.baseline_favorite_border_black_48 : R.drawable.baseline_favorite_black_48);

        holder.ivDeleteUser.setOnClickListener(v -> {
            if (onViewClickListener != null) {
                onViewClickListener.OnDeleteClick(position);
            }
        });

        holder.ivFavoriteUser.setOnClickListener(v -> {
            if (onViewClickListener != null) {
                onViewClickListener.onFavoriteClick(position);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onViewClickListener != null) {
                    onViewClickListener.OnItemClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    //
    public interface OnViewClickListener {
        void OnDeleteClick(int position);

        void onFavoriteClick(int position);

        void OnItemClick(int position);
    }

    class UserHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvFullName)
        TextView tvFullName;
        @BindView(R.id.tvLanguage)
        TextView tvLanguage;
        //        @BindView(R.id.tvDob)
//        TextView tvDob;
        @BindView(R.id.tvAge)
        TextView tvAge;
        @BindView(R.id.ivDeleteUser)
        ImageView ivDeleteUser;
        @BindView(R.id.ivFavoriteUser)
        ImageView ivFavoriteUser;

        public UserHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
